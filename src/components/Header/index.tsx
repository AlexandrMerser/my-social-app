import React, { ReactNode } from "react";
import styled from "styled-components/macro";

import Container from "../../primitives/Container";
import Logo from "../../primitives/Logo";

import {
  ai,
  ALIGNS,
  flex,
  height,
  marginRight,
  width
} from "../../helpers/styles";

import Wrapper from "../../primitives/Wrapper";

import logo from "../../icons/logo.svg";

const HeaderStyled = styled.header`
  width: 100%;
  height: 120px;
  display: flex;
  align-items: center;
`;

interface HeaderInterface {
  styles?: any[];
  children: ReactNode;
}
const Header = ({ styles, children }: HeaderInterface) => {
  return <HeaderStyled css={styles}>{children}</HeaderStyled>;
};

interface HeaderAppInterface {
  styles?: any[];
}
const HeaderApp = ({ styles }: HeaderAppInterface) => {
  return (
    <Header styles={styles}>
      <Container>
        <Wrapper styles={[flex, ai(ALIGNS.CENTER)]}>
          <Logo
            href="/"
            altText="logo"
            src={logo}
            styles={[width("100px"), height("100px"), marginRight(25)]}
          />
        </Wrapper>
      </Container>
    </Header>
  );
};

export default HeaderApp;
