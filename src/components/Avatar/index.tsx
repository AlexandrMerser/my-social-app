import React from "react";
import styled from "styled-components/macro";

import { COLOR, getColor } from "../../helpers/colors";

const StyledAvatar = styled.div`
  width: 150px;
  height: 150px;
  border: 2px solid ${getColor(COLOR.DARK_BLUE)};
  border-radius: 25px;
  overflow: hidden;

  img {
    width: 100%;
    max-width: 100%;
    height: auto;
  }
`;

interface AvatarInterface {
  styles?: any[];
  source: string;
  altText: string;
}
const Avatar = ({ styles, source, altText }: AvatarInterface) => {
  return (
    <StyledAvatar css={styles}>
      <img src={source} alt={altText} />
    </StyledAvatar>
  );
};

export default Avatar;
