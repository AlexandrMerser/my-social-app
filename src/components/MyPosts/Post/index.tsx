import React from "react";
import Block from "../../../primitives/Block";
import Typography, { TypeText } from "../../../primitives/Typography";
import Wrapper from "../../../primitives/Wrapper";
import {
  bgColor,
  borderRadius,
  color,
  FontWeight,
  fontWeight,
  marginBottom,
  padding
} from "../../../helpers/styles";
import { COLOR, getColor } from "../../../helpers/colors";

interface PostInterface {
  message: string;
  countLikes: number;
}
const Post = ({ message, countLikes }: PostInterface) => {
  return (
    <Wrapper
      styles={[
        marginBottom(5),
        padding("5px"),
        bgColor(getColor(COLOR.WHITE)),
        borderRadius("5px")
      ]}
    >
      <Block styles={[marginBottom(15)]}>
        <Typography
          type={TypeText.usual}
          styles={[
            color(getColor(COLOR.DARK_BLUE)),
            fontWeight(FontWeight.LIGHT)
          ]}
        >
          {message}
        </Typography>
      </Block>
      <Block>
        <span>Likes</span>
        <span style={{ marginLeft: "10px" }}>{countLikes}</span>
      </Block>
    </Wrapper>
  );
};

export default Post;
