import React from "react";

import Post from "./Post";

import Block from "../../primitives/Block";
import { marginBottom } from "../../helpers/styles";

const MyPosts = () => {
  return (
    <>
      <Block styles={[marginBottom(50)]}>
        <textarea name="textarea" />
        <button>Добавить пост</button>
      </Block>

      <Block>
        <Post message="Hello, bro" countLikes={23} />
        <Post message="Hello, xfdsf" countLikes={1} />
        <Post message="Hesdfllo, fdsf" countLikes={15} />
        <Post message="Helvcxvlo, bdsfro" countLikes={0} />
      </Block>
    </>
  );
};

export default MyPosts;
