import React, { ReactNode } from "react";
import styled from "styled-components/macro";
import { NavLink } from "react-router-dom";

const StyledLink = styled(NavLink)`
  padding: 10px 20px;
  margin: 10px;
  display: block;
  text-decoration: none;
  color: white;
  background: #450c65;
  border-radius: 12px;
  transition: all ease-in-out 0.1s;

  &.activeLink {
    background: #935cb2;
    transition: all ease-in-out 0.1s;
  }
  &:hover {
    background: #935cb2;
    transition: all ease-in-out 0.1s;
  }
`;

interface LinkInterface {
  path: string;
  styles?: any[];
  children: ReactNode;
}
const Link = ({ children, styles, path = "/" }: LinkInterface) => (
  <StyledLink exact to={path} css={styles} activeClassName="activeLink">
    {children}
  </StyledLink>
);

export default Link;
