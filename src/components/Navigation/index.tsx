import React, { ReactNode } from "react";
import styled from "styled-components";

import Link from "./Elements/Link";
import Typography, { TypeText } from "../../primitives/Typography";

import { margin } from "../../helpers/styles";

const StyledNav = styled.nav`
  width: 100%;
  max-width: 300px;
  min-width: 200px;
  height: calc(100vh - 200px);
`;

interface NavInterface {
  children: ReactNode;
  styles?: any[];
}
export const Nav = ({ children, styles }: NavInterface) => (
  <StyledNav css={styles}>{children}</StyledNav>
);

interface NavigationInterface {
  data: {
    id: string;
    name: string;
    active: boolean;
    path: string;
  }[];
}
const Navigation = ({ data }: NavigationInterface) => {
  return (
    <Nav>
      <ul>
        {data &&
          data.map(({ id, name, path }) => (
            <Link styles={[margin("5px")]} key={id} path={path}>
              <Typography type={TypeText.usual}>{name}</Typography>
            </Link>
          ))}
      </ul>
    </Nav>
  );
};

export default Navigation;
