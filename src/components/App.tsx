import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Profile from "../Pages/Profile";
import Dialogs from "../Pages/Dialogs";
import News from "../Pages/News";
import Music from "../Pages/Music";
import Settings from "../Pages/Settings";

import HeaderApp from "./Header";
import Navigation from "./Navigation";

import Container from "../primitives/Container";
import Wrapper from "../primitives/Wrapper";

import {
  border,
  borderRadius,
  flex,
  fullWidth,
  marginLeft,
  padding
} from "../helpers/styles";
import { COLOR, getColor } from "../helpers/colors";

const App = () => {
  const data = getData();

  return (
    <>
      <BrowserRouter>
        <HeaderApp />
        <Container>
          <Wrapper styles={[flex]}>
            <Navigation data={data} />
            <Wrapper
              styles={[
                fullWidth,
                marginLeft(50),
                border(2, getColor(COLOR.WHITE)),
                borderRadius("15px"),
                padding("25px")
              ]}
            >
              <Switch>
                <Route exact path="/" component={Profile} />
                <Route path="/dialoges" component={Dialogs} />
                <Route path="/news" component={News} />
                <Route path="/music" component={Music} />
                <Route path="/settings" component={Settings} />
              </Switch>
            </Wrapper>
          </Wrapper>
        </Container>
      </BrowserRouter>
    </>
  );
};

export default App;

const getData = () => [
  {
    name: "Профиль",
    id: "e54klfd3",
    active: true,
    path: "/"
  },
  {
    name: "Сообщения",
    id: "e54gfdsd3",
    active: false,
    path: "/dialoges"
  },
  {
    name: "Новости",
    id: "gfdg43452",
    active: false,
    path: "/news"
  },
  {
    name: "Музыка",
    id: "gfdg34213gfs",
    active: false,
    path: "/music"
  },
  {
    name: "Настройки",
    id: "432Fs32dsd",
    active: false,
    path: "/settings"
  }
];
