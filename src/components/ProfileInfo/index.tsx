import React from "react";
import Avatar from "../Avatar";

import Wrapper from "../../primitives/Wrapper";

import avatarPicture from "./images/avatarExample.jpg";
import { marginBottom } from "../../helpers/styles";

const ProfileInfo = ({ styles }: { styles?: any[] }) => {
  return (
    <>
      <Wrapper styles={styles}>
        <Avatar
          styles={[marginBottom(50)]}
          source={avatarPicture}
          altText="avatar"
        />
        <div>Description</div>
      </Wrapper>
    </>
  );
};

export default ProfileInfo;
