import React from "react";

import ProfileInfo from "../../components/ProfileInfo";
import MyPosts from "../../components/MyPosts";

import { marginBottom } from "../../helpers/styles";

const Profile = () => {
  return (
    <>
      <ProfileInfo styles={[marginBottom(20)]} />
      <MyPosts />
    </>
  );
};

export default Profile;
