import { css } from "styled-components";
import { COLOR } from "../colors";

export const flex = css`
  display: flex;
`;

export enum ALIGNS {
  CENTER = "center",
  BETWEEN = "space-between",
  START = "flex-start",
  END = "flex-end"
}
export const ai = (value: ALIGNS) =>
  css`
    align-items: ${value};
  `;

export const jc = (value: ALIGNS) =>
  css`
    justify-content: ${value};
  `;

export const width = (value: string) => {
  return css`
    width: ${value};
  `;
};

export const height = (value: string) =>
  css`
    height: ${value};
  `;

export const fullWidth = css`
  width: 100%;
`;

export const fullHeight = css`
  height: 100%;
`;

export const fz = (value: number) =>
  css`
    font-size: ${value}px;
  `;

export const block = css`
  display: block;
`;

export const inlineBlock = css`
  display: inline-block;
`;

export const borderRadius = (value: string) =>
  css`
    border-radius: ${value};
  `;

export const border = (value: number, color: string) =>
  css`
    border: ${value}px solid ${color};
  `;

export enum TEXTALIGNS {
  CENTER = "center",
  JUSTIFY = "justify",
  LEFT = "left",
  RIGHT = "right"
}
export const textAlign = (value: TEXTALIGNS) =>
  css`
    text-align: ${value};
  `;

export const margin = (value: string) =>
  css`
    margin: ${value};
  `;

export const padding = (value: string) =>
  css`
    padding: ${value};
  `;

export const paddingLeft = (value: number) =>
  css`
    padding-left: ${value}px;
  `;
export const paddingRight = (value: number) =>
  css`
    padding-right: ${value}px;
  `;
export const paddingTop = (value: number) =>
  css`
    padding-top: ${value}px;
  `;
export const paddingBottom = (value: number) =>
  css`
    padding-bottom: ${value}px;
  `;

export const marginCenter = css`
  margin: 0 auto;
`;

export const outline = (value: number, color: string) =>
  css`
    outline: ${value}px solid ${color};
  `;

export const marginLeft = (value: number) =>
  css`
    margin-left: ${value}px;
  `;

export const marginRight = (value: number) =>
  css`
    margin-right: ${value}px;
  `;

export const marginTop = (value: number) =>
  css`
    margin-top: ${value}px;
  `;

export const marginBottom = (value: number) =>
  css`
    margin-bottom: ${value}px;
  `;

export const color = (value: string) =>
  css`
    color: ${value};
  `;

export enum FontWeight {
  THIN = "100",
  LIGHT = "200",
  BOOK = "300",
  REGULAR = "400",
  MEDIUM = "500",
  SEMI_BOLD = "600",
  BOLD = "700",
  HEAVY = "800",
  ULTRA_BLACK = "900"
}
export const fontWeight = (value: FontWeight) =>
  css`
    font-weight: ${value};
  `;

export const bg = (value: string) =>
  css`
    background: ${value};
  `;

export const bgColor = (color: COLOR) =>
  css`
    background-color: ${color};
  `;

export enum BackgroundSizeInterface {
  COVER = "cover",
  CONTAIN = "contain",
  AUTO = "auto"
}
export const backgroundSize = (value: BackgroundSizeInterface | string) =>
  css`
    background-size: ${value};
  `;

export enum backgroundPositionX {
  LEFT = "left",
  CENTER = "center",
  RIGHT = "right"
}

export enum backgroundPositionY {
  TOP = "top",
  CENTER = "center",
  BOTTOM = "bottom"
}

export const backgroundPosition = (
  x: backgroundPositionX | string,
  y: backgroundPositionY | string
) =>
  css`
    background-position: ${x}, ${y};
  `;
