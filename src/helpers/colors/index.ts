export enum COLOR {
  BLACK = "black",
  WHITE = "white",
  RED = "red",
  GREEN = "green",
  DARK_BLUE = "darkblue",
  BLUE = "blue",
  AQUA = "aqua"
}
export const getColor = (value: COLOR) => value;
