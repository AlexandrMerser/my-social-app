import React, { ReactNode } from "react";
import styled from "styled-components/macro";

const StyledBlock = styled.div``;

interface BlockInterface {
  children: ReactNode;
  styles?: any[];
}
const Block = ({ styles, children }: BlockInterface) => (
  <StyledBlock css={styles}>{children}</StyledBlock>
);

export default Block;
