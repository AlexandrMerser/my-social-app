import React from "react";
import { NavLink } from "react-router-dom";
import styled from "styled-components/macro";

const LogoLink = styled(NavLink)`
  display: block;
  cursor: pointer;
  text-decoration: none;
`;

interface LogoInterface {
  styles?: any[];
  href: string;
  src: string;
  altText: string;
}
const Logo = ({ styles, src, altText, href }: LogoInterface) => {
  return (
    <LogoLink to={href} css={styles}>
      <img src={src} alt={altText} />
    </LogoLink>
  );
};

export default Logo;
