import React, { ReactElement, ReactNode } from "react";
import styled from "styled-components/macro";

const H1 = styled.h1`
  font-family: "Arial", sans-serif;
  font-size: 30px;
  font-weight: 500;
`;

const H2 = styled.h2`
  font-family: "Arial", sans-serif;
  font-size: 22px;
  font-weight: 400;
`;

const H3 = styled.h3`
  font-family: "Arial", sans-serif;
  font-size: 18px;
  font-weight: 300;
`;

const H4 = styled.h4`
  font-family: "Arial", sans-serif;
  font-size: 16px;
  font-weight: 200;
`;

const H5 = styled.h5`
  font-family: "Arial", sans-serif;
  font-size: 14px;
  font-weight: 200;
`;

const H6 = styled.h6`
  font-family: "Arial", sans-serif;
  font-size: 13px;
  font-weight: 200;
`;

const Text = styled.span`
  font-family: "Calibri Light", sans-serif;
  font-size: 16px;
  font-weight: 300;
`;

export enum TypeText {
  H1 = "title1",
  H2 = "title2",
  H3 = "title3",
  H4 = "title4",
  H5 = "title5",
  H6 = "title6",
  usual = "textUsual"
}

type MapTextTitleInterface = {
  [key in TypeText]: (children: ReactNode, styles?: any[]) => ReactElement;
};

const mapTextTitle: MapTextTitleInterface = {
  title1: (children, styles) => <H1 css={styles}>{children}</H1>,
  title2: (children, styles) => <H2 css={styles}>{children}</H2>,
  title3: (children, styles) => <H3 css={styles}>{children}</H3>,
  title4: (children, styles) => <H4 css={styles}>{children}</H4>,
  title5: (children, styles) => <H5 css={styles}>{children}</H5>,
  title6: (children, styles) => <H6 css={styles}>{children}</H6>,
  textUsual: (children, styles) => <Text css={styles}>{children}</Text>
};

interface TypographyInterface {
  type: TypeText;
  children: ReactNode;
  styles?: any[];
}

const Typography = ({ type, children, styles }: TypographyInterface) =>
  mapTextTitle[type](children, styles);

export default Typography;
