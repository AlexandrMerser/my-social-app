import React, {ReactNode} from 'react';
import styled from 'styled-components/macro';

const WrapperDiv = styled.div``;

interface WrapperInterface {
    children: ReactNode;
    styles?: any[];
}
const Wrapper = ({children, styles}: WrapperInterface) => {
    return <WrapperDiv css={styles}>{children}</WrapperDiv>
};

export default Wrapper;