import React, { ReactNode } from "react";
import styled from "styled-components";

const ContainerStyled = styled.div`
  width: 100%;
  max-width: 1300px;
  height: auto;
  margin: 0 auto;
`;

interface ContainerInterface {
  children: ReactNode;
  styles?: any[];
}
const Container = ({ children, styles }: ContainerInterface) => {
  return <ContainerStyled css={styles}>{children}</ContainerStyled>;
};

export default Container;
